provider "aws" {
	region = "us-east-1"
}

//Subo claves publicas a AWS para ser consumida desde EC2
resource "aws_key_pair" "key_administracion" {
  key_name   = var.key_administracion
  public_key = var.key_administracion_pubkey
}

//Genero vpc completo para generar varios ambientes con mismo código
module "network" {
  source                    = "./modules/network"
  vpc_net                   = var.vpc_net
  availability_zones        = var.availability_zones
  availability_zone_subnet  = var.availability_zone_subnet
}

//Genero los grupos de seguridad para un balanceador ALB
module "security_group_alb" {
  source      = "./modules/security_group_alb"
  vpc_id      = module.network.vpc_id
  sg_name_alb = var.sg_name_alb
  sg_name_ec2 = var.sg_name_ec2
}

//Genero el balanceador ALB basado en dos instancias de EC2
module "infrastructure" {
  source             = "./modules/infrastructure"
  vpc_id             = module.network.vpc_id
  sg_alb_id          = module.security_group_alb.sg_alb_id
  sg_ec2_admin_id    = module.security_group_alb.sg_admin_ec2_id
  sg_ec2_alb_id      = module.security_group_alb.sg_alb_ec2_id
  ec2_instance_1     = var.ec2_1
  ec2_instance_2     = var.ec2_2
  key_administracion = var.key_administracion
}