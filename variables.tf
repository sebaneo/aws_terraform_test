variable "vpc_net" {
	description = "Red VPC principal"
	default     = "10.1.0.0/16"
}

variable "availability_zones" {
  default = [
    "us-east-1a",
    "us-east-1b",
    "us-east-1c",
    "us-east-1d",
    "us-east-1e",
    "us-east-1f"
  ]
}

variable "availability_zone_subnet" {
  default = [
    "10.1.1.0/24",
    "10.1.2.0/24",
    "10.1.3.0/24",
    "10.1.4.0/24",
    "10.1.5.0/24",
    "10.1.6.0/24"
  ]
}

variable "key_administracion" {
	description = "Nombre key pública acceso ssh"
	default     = "key_administracion"
}

variable "key_administracion_pubkey" {
	description = "Clave pública acceso"
	default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC+7e1LLHCU+lGzwxP0s1ebCOf6hprGATWSENKjLNjNa3qykuhrsm88SIhVaY40cdx+exGrmXZO+pQ2hf/dAvoaiAzr3Nz17CDJosvCvnO5YoRASFLj3iTlqgk61V/eqFupgPcM7VW/cMPI4+4Nyr/vbogBYuaSmDmOsR9m7eFc4K4qZyQISvCBknG/Jtp7Z/eAPQsP2Fw9itGYDjJ79I2x6hXkEAhlXJ23OEii6x0ufQwwQdNsst6ssOfUEt6rnTyqc9y7yb8/oxuSgjqh6wKGMZvZLbzztdcBY5/cUGZizFvnOTECzs7cfCZChl/JHnX9W/9ZOmSXxzoMRqb1rd0J sebastian@minote"
}

variable "sg_name_alb" {
  description = "Nombre del security group utilizado para el alb"
  default     = "sg_alb_terraform"
}

variable "sg_name_ec2" {
  description = "Nombre del secutiry group utilizado para las instancia del target del alb"
  default     = "sg_alb_ec2"
}

variable "ec2_1" {
  description = "Parámetros primer instancia EC2"
  default = {
    name  = "apache"
    zone  = "us-east-1b"
    image = "ami-0323c3dd2da7fb37d"
    type  = "t2.micro"
    user_data_file = "files/httpd.tpl"
  }
}

variable "ec2_2" {
  description = "Parámetros primer instancia EC2"
  default = {
    name  = "nginx"
    zone  = "us-east-1a"
    image = "ami-0323c3dd2da7fb37d"
    type  = "t2.micro"
    user_data_file = "files/nginx.tpl"
  }
}

