//Genero vpc, subredes, rutas y gateway de salida a internet y asociolas subredes
resource "aws_vpc" "default" {
  cidr_block = var.vpc_net
  tags = {
    Name = "vpc ${var.vpc_net}"
  }
}

resource "aws_subnet" "subnet_principal" {
  count             = length(var.availability_zones)
  availability_zone = var.availability_zones[count.index]
  vpc_id            = aws_vpc.default.id
  cidr_block        = var.availability_zone_subnet[count.index]

  tags = {
      Name = "Subnet publica ${var.availability_zones[count.index]}" 
  }
}

resource "aws_internet_gateway" "default" {
  vpc_id = aws_vpc.default.id

  tags = {
      Name = "Gateway vpc principal"
  }
}

resource "aws_route_table" "ruta_publica" {
  vpc_id = aws_vpc.default.id

  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.default.id
  }

  tags = {
      Name = "Ruta internet"
  }
}

resource "aws_route_table_association" "acceso_publico" {
  count          = length(var.availability_zones)
  subnet_id      = element(aws_subnet.subnet_principal.*.id, count.index)
  route_table_id = aws_route_table.ruta_publica.id
}

resource "aws_default_security_group" "default" {
  vpc_id = aws_vpc.default.id

  ingress {
    protocol  = -1
    self      = true
    from_port = 0
    to_port   = 0
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}