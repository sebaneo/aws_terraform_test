output "vpc_id" {
    description = "ID del VPC generado"
    value       = aws_vpc.default.id

    depends_on = [aws_subnet.subnet_principal]
}