variable "vpc_net" {
  description = "Red a tomar por el vpc"
  type        = string
}

variable "availability_zones" {
  description = "Zonas de disponibilidad a manejar"
  type        = list
}

variable "availability_zone_subnet" {
  description = "Subredes disponibles para zonas de disponibilidad"
  type        = list
}