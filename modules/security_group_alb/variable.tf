variable "vpc_id" {
  description = "Id del vpc sobre el cual se va a crear el SG"
  type        = string
}
variable "sg_name_alb" {
  description = "Nombre del sg asignado al ALB"
  type        = string
}
variable "sg_name_ec2" {
  description = "Nombre del sg asignado a las instancia del target ALB"
  type        = string
}