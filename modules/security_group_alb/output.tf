output "sg_alb_id" {
  description = "ID del sg asociado al ALB"
  value       = aws_security_group.aplicativo_web_alb.id
}

output "sg_alb_ec2_id" {
  description = "ID del sg asociado al las instancias ec2 del ALB"
  value       = aws_security_group.aplicativo_web_ec2.id
}

output "sg_admin_ec2_id" {
  description = "ID del sg para tareas de administración SSH asociado al las instancias ec2 del ALB"
  value       = aws_security_group.acceso_admin.id
}