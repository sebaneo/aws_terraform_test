//Genero los security groups de ALB e instancias EC2
resource "aws_security_group" "aplicativo_web_alb" {
  name        = var.sg_name_alb
  description = "Ports acceso webserver en ALB"
  vpc_id      = var.vpc_id
}

resource "aws_security_group" "aplicativo_web_ec2" {
name        = var.sg_name_ec2
description = "Ports acceso webserver EC2"
vpc_id      = var.vpc_id
}
//Genero reglas de los SG. Se hace separado ya que dependen entre si y necesitan estar creados
resource "aws_security_group" "acceso_admin" {
  name        ="Acceso administracion"
  description = "Port acceso SSH"
  vpc_id      = var.vpc_id
  
  ingress {
    from_port   = 0
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  egress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group_rule" "alb_inet_input" {
  type              = "ingress"
  from_port         = 80 
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.aplicativo_web_alb.id
}

resource "aws_security_group_rule" "alb_to_ec2" {
  type                     = "egress"
  from_port                = 80 
  to_port                  = 80
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.aplicativo_web_ec2.id
  security_group_id        = aws_security_group.aplicativo_web_alb.id
}

resource "aws_security_group_rule" "ec2_alb_input" {
  type                     = "ingress"
  from_port                = 80 
  to_port                  = 80
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.aplicativo_web_alb.id
  security_group_id        = aws_security_group.aplicativo_web_ec2.id
}

resource "aws_security_group_rule" "ec2_to_repo_http" {
  type              = "egress"
  from_port         = 80 
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.aplicativo_web_ec2.id
}

resource "aws_security_group_rule" "ec2_to_repo_https" {
  type              = "egress"
  from_port         = 443 
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.aplicativo_web_ec2.id
}