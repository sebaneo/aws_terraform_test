//Archivos para instalar servidores durante generación instancia
data "template_file" "data_instance_1" {
  template = file("${path.cwd}/${var.ec2_instance_1["user_data_file"]}")
}

data "template_file" "data_instance_2" {
  template = file("${path.cwd}/${var.ec2_instance_2["user_data_file"]}")
}

//Recupero subred publica perteneciente a availability zone de la instancia
data "aws_subnet_ids" "subnets_1" {
  vpc_id = var.vpc_id

  tags = {
    Name = "Subnet publica ${var.ec2_instance_1["zone"]}"
  }
}

data "aws_subnet_ids" "subnets_2" {
  vpc_id = var.vpc_id

  tags = {
    Name = "Subnet publica ${var.ec2_instance_2["zone"]}"
  }
}

//Genero las instancias
resource "aws_instance" "instance_1" {
  ami                         = var.ec2_instance_1["image"]
  availability_zone           = var.ec2_instance_1["zone"]
  subnet_id                   = element(tolist(data.aws_subnet_ids.subnets_1.ids), 0)
  instance_type               = var.ec2_instance_1["type"]
  key_name                    = var.key_administracion
  vpc_security_group_ids      = [var.sg_ec2_admin_id, var.sg_ec2_alb_id]
  associate_public_ip_address = true
  user_data                   = data.template_file.data_instance_1.template

  tags = {
    Name = "server ${var.ec2_instance_1["name"]}"
  }
}

resource "aws_instance" "instance_2" {
  ami                         = var.ec2_instance_2["image"]
  availability_zone           = var.ec2_instance_2["zone"]
  subnet_id                   = element(tolist(data.aws_subnet_ids.subnets_2.ids), 0)
  instance_type               = var.ec2_instance_2["type"]
  key_name                    = var.key_administracion
  vpc_security_group_ids      = [var.sg_ec2_admin_id, var.sg_ec2_alb_id]
  associate_public_ip_address = true
  user_data                   = data.template_file.data_instance_2.template

  tags = {
    Name = "server ${var.ec2_instance_2["name"]}"
  }
}

//Genero target y adjunto las instancias EC2 al mismo
resource "aws_lb_target_group" "web-target-group" {
    name        = "web-target-group"
    port        = 80
    protocol    = "HTTP"
    target_type = "instance"
    vpc_id      = var.vpc_id

    health_check {
        interval            = 10
        path                = "/check.html"
        protocol            = "HTTP"
        timeout             = 5
        healthy_threshold   = 5
        unhealthy_threshold = 2
     }
}

resource "aws_lb_target_group_attachment" "lb-target-attachment-nginx" {
  target_group_arn  = aws_lb_target_group.web-target-group.arn
  target_id         = aws_instance.instance_1.id
  port              = 80
}

resource "aws_lb_target_group_attachment" "lb-target-attachment-apache" {
  target_group_arn  = aws_lb_target_group.web-target-group.arn
  target_id         = aws_instance.instance_2.id
  port              = 80
}

//Genero el balanceador y su correspondiente listener
resource "aws_lb" "web-alb" {
  name                = "web-alb"
  internal            = false
  ip_address_type     = "ipv4"
  load_balancer_type  = "application"

  security_groups = [
      var.sg_alb_id
  ]

  subnets = [
    element(tolist(data.aws_subnet_ids.subnets_1.ids), 0),
    element(tolist(data.aws_subnet_ids.subnets_2.ids), 0),
  ]

  tags = {
      Name = "web-alb"
  }
}

resource "aws_lb_listener" "my-test-alb-listner" {
    load_balancer_arn = aws_lb.web-alb.arn
    port              = 80
    protocol          = "HTTP"

    default_action {
        type              = "forward"
        target_group_arn  = aws_lb_target_group.web-target-group.arn
    }
}
