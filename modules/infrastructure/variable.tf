variable "vpc_id" {
    description = "Id del vpc sobre el cual se va a crear el SG"
    type        = string
}

variable "ec2_instance_1" {
    description = "Instancia 1 perteneciente al target de ALB"
    type        = map
}

variable "ec2_instance_2" {
    description = "Instancia 2 perteneciente al target de ALB"
    type        = map
}

variable "sg_alb_id" {
    description = "Id del sg asociado al ALB"
    type        = string
}

variable "sg_ec2_admin_id" {
    description = "Id del sg que brinda acceso administración SSH"
    type        = string
}

variable "sg_ec2_alb_id" {
    description = "Id del sg que permite acceso a la instancia al ALB"
    type        = string
}

variable "key_administracion" {
    description = "Key publica almacenada que se utilizará para validar acceso SSH"
    type        = string
}